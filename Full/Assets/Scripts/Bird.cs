﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Bird : MonoBehaviour
{
    public enum Status
    {
        Stay,Fly,Grounded
    }

    [SerializeField]
    float minPower = 1.0f;

    [SerializeField]
    float maxPower = 25.0f;

    Rigidbody2D rigid;

    PredictionLine predict;

    float screenDec = 0.05f;

    Vector3 startPosition;

    Status status = Status.Stay;

    private void Start()
    {
        var input = GameObject.FindObjectOfType<DragInput>();
        input.OnEndDragEvent += this.OnEndDrag;
        input.OnMoveDragEvent += this.OnMoveDrag;
        input.OnBeginDrag += this.OnBeginDrag;

        rigid = GetComponent<Rigidbody2D>();

        predict = GetComponent<PredictionLine>();

        startPosition = transform.position;
    }

    private void Update()
    {
        switch(status)
        {
            case Status.Fly:
                {
                    RotToForward(rigid.velocity);
                }
                break;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (status == Status.Fly)
        {
            var manager = GameObject.FindObjectOfType<GameManager>();
            manager.OnGroundBird();

            if (manager.BirdCount > 0)
            {
                Respawn();
            }

            status = Status.Grounded;
        }
    }

    void RotToForward(Vector3 forward)
    {
        if (forward.magnitude > 0.1f)
        {
            Quaternion look = Quaternion.FromToRotation(Vector3.right, (Vector3)forward.normalized);
            rigid.MoveRotation(look.eulerAngles.z);
        }
    }

    void OnEndDrag(Vector2 start,Vector2 end)
    {
        //---------------------------------------------------------
        //ここに鳥を飛ばすコードを記述
        //---------------------------------------------------------
        if (status != Status.Stay)
            return;

        Vector2 dir = start - end;
        float dirLength = dir.magnitude * screenDec;
        dir.Normalize();

        if (dirLength < minPower)
            return;

        if (dirLength > maxPower)
            dirLength = maxPower;

        predict.SetDotVisible(false);

        rigid.gravityScale = 1.0f;
        rigid.AddForce(dir * dirLength, ForceMode2D.Impulse);

        status = Status.Fly;
    }

    void Respawn()
    {
        var player = Resources.Load("Bird") as GameObject;
        Instantiate(player, startPosition, Quaternion.identity);
    }

    void OnMoveDrag(Vector2 start, Vector2 end)
    {
        if (status != Status.Stay)
            return;

        Vector2 dir = start - end;
        float dirLength = dir.magnitude * screenDec;
        dir.Normalize();

        if (dirLength < minPower)
            return;

        if (dirLength > maxPower)
            dirLength = maxPower;

        predict.SetPredictDotAll(dir * dirLength);

        RotToForward(dir);
    }

    void OnBeginDrag(Vector2 begin)
    {
        if (status != Status.Stay)
            return;

        predict.SetDotVisible(true);
    }
}
