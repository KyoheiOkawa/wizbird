﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider2D))]
public class ColliderLink : MonoBehaviour
{
    [SerializeField]
    SpriteRenderer spriteRenderer;
    [SerializeField]
    BoxCollider2D boxCollider;

    private void Reset()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        boxCollider = GetComponent<BoxCollider2D>();
    }

    public void Link()
    {
        boxCollider.size = spriteRenderer.size;
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(ColliderLink))]
    public class ColliderLinkEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            ColliderLink cl = target as ColliderLink;

            if(GUILayout.Button("Link"))
            {
                cl.Link();
            }
        }
    }
#endif
}
