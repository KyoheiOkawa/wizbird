﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragInput : MonoBehaviour
{
    public delegate void DragBeginDelegate(Vector2 begin);
    public delegate void DragDelegate(Vector2 start, Vector2 end);

    public enum Status
    {
        None,Move
    }

    Status status = Status.None;

    Vector2 dragStartPoint;

    Vector2 dragEndPoint;

    public event DragBeginDelegate OnBeginDrag = delegate (Vector2 begin) { };
    public event DragDelegate OnMoveDragEvent = delegate (Vector2 start, Vector2 end) { };
    public event DragDelegate OnEndDragEvent = delegate(Vector2 start, Vector2 end) { };

    private void Update()
    {
        if (GameObject.FindObjectOfType<GameManager>().IsFinishedGame)
            return;

        switch(status)
        {
            case Status.None:
                None();
                break;
            case Status.Move:
                Move();
                break;
        }
    }

    void None()
    {
        if(Input.GetMouseButtonDown(0))
        {
            status = Status.Move;
            dragStartPoint = Input.mousePosition;
            OnBeginDrag(dragStartPoint);
        }
    }

    void Move()
    {
        if(Input.GetMouseButtonUp(0))
        {
            status = Status.None;
            dragEndPoint = Input.mousePosition;

            OnEndDragEvent(dragStartPoint, dragEndPoint);
        }
        else
        {
            OnMoveDragEvent(dragStartPoint, Input.mousePosition);
        }
    }
}
