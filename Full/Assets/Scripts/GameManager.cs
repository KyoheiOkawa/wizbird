﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using System;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    int maxBirdCount = 3;

    [SerializeField]
    Text leftBirdText;

    bool isFinishedGame = false;

    public bool IsFinishedGame
    {
        get
        {
            return isFinishedGame;
        }
    }

    int enemyCount = 0;

    int birdCount;

    public int BirdCount { get { return birdCount; } }

    GameObject[] obstacles;

    [SerializeField]
    float waitToStationary = 2.0f;//何秒静止状態を保ってからゲームオーバーになるか

    float stationaryCount = 0.0f;

    private void Start()
    {
        var enemies = GameObject.FindGameObjectsWithTag("Enemy");
        enemyCount = enemies.Length;

        obstacles = GameObject.FindGameObjectsWithTag("Obstacle");

        birdCount = maxBirdCount;
    }

    private void Update()
    {
        if (leftBirdText)
            leftBirdText.text = birdCount.ToString();

        if (IsFinishedGame)
            return;

        if(birdCount <= 0)
        {
            bool isExistDynamic = false;

            Func<GameObject,bool> checkExist = o => {
                var rigid = o.GetComponent<Rigidbody2D>();
                return rigid.velocity.magnitude > 0.1f || rigid.angularVelocity > 0.1f;
            };

            isExistDynamic = obstacles.Any(checkExist);
            isExistDynamic = GameObject.FindGameObjectsWithTag("Enemy").Any(checkExist);
            isExistDynamic = GameObject.FindGameObjectsWithTag("Player").Any(checkExist);

            if (!isExistDynamic)
            {
                stationaryCount += Time.deltaTime;

                if (stationaryCount > waitToStationary)
                {
                    Fail();
                    isFinishedGame = true;
                }
            }
            else
            {
                stationaryCount = 0.0f;
            }
        }
    }

    public void OnGroundBird()
    {
        birdCount--;
    }

    public void OnDeadEnemy()
    {
        enemyCount--;

        if(enemyCount <= 0)
        {
            isFinishedGame = true;
            Invoke("Clear", 1.0f);   
        }
    }

    void Clear()
    {
        var canvasTrans = GameObject.Find("Canvas").transform;
        var clearPanel = Resources.Load("ClearPanel");
        Instantiate(clearPanel, canvasTrans);
    }

    void Fail()
    {
        var canvasTrans = GameObject.Find("Canvas").transform;
        var clearPanel = Resources.Load("FailedPanel");
        Instantiate(clearPanel, canvasTrans);
    }
}
