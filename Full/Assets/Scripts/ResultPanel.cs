﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResultPanel : MonoBehaviour
{
    public void OnRetryButton()
    {
        var nowScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(nowScene.name);
    }
}
