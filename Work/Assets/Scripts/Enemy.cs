﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Enemy : MonoBehaviour
{
    public enum Status
    {
        Alive,Dead
    }

    [SerializeField]
    float deadFallVelocity = 1.0f;

    Rigidbody2D rigid;

    Status status = Status.Alive;

    private void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
    }

    void Dead()
    {
        if (status != Status.Alive)
            return;

        GameObject.FindObjectOfType<GameManager>().OnDeadEnemy();

        var child = transform.Find("Cloud");
        child.parent = null;
        child.GetComponent<Animator>().SetTrigger("Exp");
        Destroy(this.gameObject);

        status = Status.Dead;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //---------------------------------------------------------
        //ここにコードを敵の死亡条件を記述
        //---------------------------------------------------------
    }
}
