﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PredictionLine : MonoBehaviour
{
    [SerializeField]
    public int countOfDot = 8;

    [SerializeField]
    float maxPredictionTime = 0.75f;

    float lerpValue = 0.0f;

    List<GameObject> dotList = new List<GameObject>();

    private void Start()
    {
        for(int i = 0; i < countOfDot; i++)
        {
            var pref = Resources.Load("dot") as GameObject;
            var obj = Instantiate(pref,transform.position,Quaternion.identity);
            dotList.Add(obj);
        }

        SetDotVisible(false);
    }

    private void Update()
    {
        lerpValue += Time.deltaTime;
        if (lerpValue > 1.0f)
            lerpValue = 0.0f;
    }

    public void SetPredictDotAll(Vector2 velocity)
    {
        float time = maxPredictionTime / (float)countOfDot;
        for (int i = 0; i < dotList.Count; i++)
        {
            float lerpTime = Mathf.Lerp(time * i, time * (i + 1), lerpValue);
            SetPredictDot(velocity, lerpTime, i);


            float alpha = 1.0f - (lerpTime / (maxPredictionTime + time));
            var renderer = dotList[i].GetComponent<SpriteRenderer>();
            Color col = renderer.color;
            col.a = alpha;
            renderer.color = col;
        }
    }

    public void SetDotVisible(bool b)
    {
        for(int i = 0; i < dotList.Count; i++)
        {
            dotList[i].GetComponent<SpriteRenderer>().enabled = b;
        }
    }

    void SetPredictDot(Vector2 velocity, float time,int index)
    {
        Vector2 result;
        result.x = transform.position.x + PredictX(velocity, time);
        result.y = transform.position.y + PredictY(velocity, time);

        dotList[index].transform.localPosition = result;
    }

    float PredictX(Vector2 velocity,float time)
    {
        return velocity.x * time;
    }

    float PredictY(Vector2 velocity, float time)
    {
        float gravity = Physics2D.gravity.y;
        return velocity.y * time + (gravity * Mathf.Pow(time, 2)) * 0.5f;
    }
}
